package de.jmizv.scrobblemdr.model;

import de.jmizv.scrobblemdr.entity.SongEntity;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.*;

@SpringBootTest
class ScrobbleToSendTest {

    @Test
    void should_create() {
        SongEntity entity = new SongEntity();
        entity.setTontraeger("He\"s very cool.");
        ScrobbleToSend of = ScrobbleToSend.of(entity);
        assertThat(of.getAlbum()).isEqualTo("He's very cool.");
    }

    @Test
    void should_convert_duration_to_seconds() {
        assertThatThrownBy(() -> ScrobbleToSend.durationAsLong(null)).isInstanceOf(NullPointerException.class);
        assertThatThrownBy(() -> ScrobbleToSend.durationAsLong("")).isInstanceOf(IllegalArgumentException.class);
        assertThatThrownBy(() -> ScrobbleToSend.durationAsLong("36")).isInstanceOf(ArrayIndexOutOfBoundsException.class);
        assertThatThrownBy(() -> ScrobbleToSend.durationAsLong(":36")).isInstanceOf(ArrayIndexOutOfBoundsException.class);
        assertThatThrownBy(() -> ScrobbleToSend.durationAsLong("1:36")).isInstanceOf(ArrayIndexOutOfBoundsException.class);
        assertThatThrownBy(() -> ScrobbleToSend.durationAsLong("01:36")).isInstanceOf(ArrayIndexOutOfBoundsException.class);
        assertThatThrownBy(() -> ScrobbleToSend.durationAsLong(":01:36")).isInstanceOf(NumberFormatException.class);

        assertThat(ScrobbleToSend.durationAsLong("0:01:36")).isEqualTo(96L);
        assertThat(ScrobbleToSend.durationAsLong("00:01:36")).isEqualTo(96L);
        assertThat(ScrobbleToSend.durationAsLong("00:1:36")).isEqualTo(96L);

        assertThat(ScrobbleToSend.durationAsLong("00:00:00")).isEqualTo(0L);
        assertThat(ScrobbleToSend.durationAsLong("99:99:99")).isEqualTo(99 + 60 * 99 + 60 * 60 * 99L);
    }

    @Test
    void should_create_map() {
        assertThatThrownBy(() -> ScrobbleToSend.mapOf("")).isInstanceOf(IllegalArgumentException.class);
        assertThatThrownBy(() -> ScrobbleToSend.mapOf("1", "A", "1", "B")).isInstanceOf(IllegalArgumentException.class);

        Map<String, String> nullMap = new HashMap<>();
        nullMap.put(null, null);
        assertThat(ScrobbleToSend.mapOf(null, null)).isEqualTo(nullMap);

        assertThat(ScrobbleToSend.mapOf()).isEqualTo(new HashMap<>());
        assertThat(ScrobbleToSend.mapOf("abc", "ZYX")).isEqualTo(Map.of("abc", "ZYX"));
        assertThat(ScrobbleToSend.mapOf("abc", "ZYX", "123","098")).isEqualTo(Map.of("abc", "ZYX", "123", "098"));
    }

    @Test
    void should_fix_spelling () {
        assertThat(ScrobbleToSend.fixSpelling(null)).isNull();
        assertThat(ScrobbleToSend.fixSpelling(" what ever ")).isEqualTo(" what ever ");
        assertThat(ScrobbleToSend.fixSpelling(" \"what ever\" ")).isEqualTo(" \"what ever\" ");
        assertThat(ScrobbleToSend.fixSpelling(" \"what \" ever\" ")).isEqualTo(" \"what \" ever\" ");
        assertThat(ScrobbleToSend.fixSpelling(" what ' ever ")).isEqualTo(" what ' ever ");
        assertThat(ScrobbleToSend.fixSpelling(" \"74 - \"75 ")).isEqualTo(" '74 - '75 ");
    }
}