package de.jmizv.scrobblemdr.repository;

import de.jmizv.scrobblemdr.entity.SongEntity;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.time.Instant;

public interface SongRepository extends CrudRepository<SongEntity, Long> {

    @Override
    SongEntity save(SongEntity entity);

    @Query(nativeQuery = true, value= "SELECT max(starttime) FROM song WHERE welle_fk_id = ?1")
    Instant findMaxStarttime(int welleId);

    Iterable<SongEntity> findByScrobbledAndWelleFkId(boolean scrobbled, int welleFkId, Sort starttime);
}
