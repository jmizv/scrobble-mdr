package de.jmizv.scrobblemdr;

import de.jmizv.scrobblemdr.entity.SongEntity;
import de.jmizv.scrobblemdr.model.ScrobbleToSend;
import de.jmizv.scrobblemdr.model.Song;
import de.jmizv.scrobblemdr.model.Welle;
import de.jmizv.scrobblemdr.model.json.ScrobbleResponse;
import de.jmizv.scrobblemdr.service.LastFmApiService;
import de.jmizv.scrobblemdr.service.TitleService;
import org.apache.commons.cli.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.time.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

@SpringBootApplication
public class MainCli implements CommandLineRunner {

    private final TitleService _titleService;
    private final LastFmApiService _lastFmApiService;

    public MainCli(@Autowired TitleService titleService, @Autowired LastFmApiService lastFmApiService) {
        _titleService = titleService;
        _lastFmApiService = lastFmApiService;
    }

    @Override
    public void run(String... args) throws Exception {
        Options options = new Options();
        options.addOption(Option.builder("welle").hasArg().desc("The Welle (ger. broadcast) for which the scrobbling should be done. One of: \"MDR Sachsen-Anhalt\", \"MDR Sachsen\", \"MDR Thüringen\".").build());
        options.addOption(Option.builder("welleId").hasArg().desc("The id of the Welle (ger. broadcast) for which the scrobbling should be done.").build());
        options.addOption(new Option("retrieveTracks", "Download the track information from the official MDR web pages."));
        options.addOption(new Option("scrobbleTracks", "Scrobble the tracks from the database."));
        options.addOption(new Option("help", "Prints this help"));
        options.addOption(Option.builder("authToken").build());
        CommandLineParser parser = new DefaultParser();
        CommandLine parse = parser.parse(options, args);

        List<Welle> wellenToConsider;
        if (parse.hasOption("welleId")) {
            String welleId = parse.getOptionValue("welleId");
            try {
                wellenToConsider = List.of(Welle.ofId(Integer.parseInt(welleId)));
            } catch (RuntimeException ex) {
                System.err.println(ex.getMessage());
                printUsage(options);
                return;
            }
        } else if (parse.hasOption("welle")) {
            try {
                wellenToConsider = List.of(Welle.ofName(parse.getOptionValue("welle")));
            } catch (RuntimeException ex) {
                System.err.println(ex.getMessage());
                printUsage(options);
                return;
            }
        } else {
            // here use all
            wellenToConsider = Arrays.asList(Welle.values());
        }
        byte retrieveAndScrobble = 0;
        if (parse.hasOption("retrieveTracks")) {
            retrieveAndScrobble |= 0B01;
        }
        if (parse.hasOption("scrobbleTracks")) {
            retrieveAndScrobble |= 0B10;
        }
        if (retrieveAndScrobble == 0) {
            retrieveAndScrobble = 0B11;
        }
        for (Welle welle : wellenToConsider) {
            try {
                if ((retrieveAndScrobble & 0B01) != 0) {
                    retrieveTracks(welle);
                }
                if ((retrieveAndScrobble & 0B10) != 0) {
                    scrobbleTracks(welle);
                }
            } catch (RuntimeException exception) {
                exception.printStackTrace(System.err);
            }
        }
    }

    private void scrobbleTracks(Welle welle) throws IOException, InterruptedException {
        Iterator<SongEntity> songsToScrobble = _titleService.getNotScrobbledTracks(welle).iterator();
        // 50 is the maximum.
        int itemsToSend = 50;
        List<ScrobbleToSend> scrobbles = new ArrayList<>(itemsToSend);
        List<Long> idsThatHasBeenScrobbled = new ArrayList<>();
        List<Long> ignoredScrobbles = new ArrayList<>();
        while (songsToScrobble.hasNext()) {
            var nextSongEntity = songsToScrobble.next();
            if (nextSongEntity.isScrobbled()) {
                continue;
            }
            scrobbles.add(ScrobbleToSend.of(nextSongEntity));
            idsThatHasBeenScrobbled.add(nextSongEntity.getId());
            if (scrobbles.size() >= itemsToSend) {
                ScrobbleResponse scrobbleResponse = _lastFmApiService.scrobble(welle, scrobbles);
                _titleService.setScrobbled(idsThatHasBeenScrobbled);
                _titleService.setIgnored(ignoredScrobbles);
                scrobbles.clear();
                scrobbleResponse.listCorrections();
                if (!scrobbleResponse.ignoredScrobbles().isEmpty()) {
                    System.err.println("Ignored Scrobbles:\n" + scrobbleResponse.ignoredScrobbles());
                    break;
                }
            }
        }
        if (!scrobbles.isEmpty()) {
            ScrobbleResponse scrobbleResponse = _lastFmApiService.scrobble(welle, scrobbles);
            _titleService.setScrobbled(idsThatHasBeenScrobbled);
            _titleService.setIgnored(ignoredScrobbles);
            scrobbles.clear();
            scrobbleResponse.listCorrections();
            if (!scrobbleResponse.ignoredScrobbles().isEmpty()) {
                System.err.println("Ignored Scrobbles:\n" + scrobbleResponse.ignoredScrobbles());
            }
        }
    }

    private void retrieveTracks(Welle welle) throws IOException, InterruptedException {
        LocalDate maxStartTimeOfTitle = _titleService.getMaxStarttimeOfTitle(welle);
        LocalDate startTimeFromDatabase;
        if (maxStartTimeOfTitle == null) {
            // Let's go back at most 2 weeks if we have no max date from the database
            startTimeFromDatabase = LocalDate.now().minusWeeks(2);
        } else {
            startTimeFromDatabase = maxStartTimeOfTitle.plusDays(1);
        }
        for (; startTimeFromDatabase.isBefore(LocalDate.now()); startTimeFromDatabase = startTimeFromDatabase.plusDays(1)) {
            int max = 500;
            List<Song> songs = _titleService.songs(welle, max, startTimeFromDatabase);
            if (songs.size() >= max) {
                // just to be sure that we've got all songs! If we got the maximum we might have missed some.
                throw new IllegalStateException("Found more than or equal to " + max + " songs.");
            }
            // persist the found songs.
            songs.stream().map((Song song) -> SongEntity.of(song, welle.getId())).forEach(_titleService::create);
        }
    }

    private static void printUsage(Options options) {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("scrobble-mdr", options);
    }

    public static void main(String[] args) {
        SpringApplication.run(MainCli.class, args);
    }
}