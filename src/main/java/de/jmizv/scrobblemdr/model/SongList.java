package de.jmizv.scrobblemdr.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Map;

public class SongList implements Serializable {

    @SerializedName("Resulttype")
    private String _resultType;
    @SerializedName("Info")
    private String _info;

    @SerializedName("Songs")
    private Map<String, Song> _songs;

    public SongList() {
    }

    public String getResultType() {
        return _resultType;
    }

    public void setResultType(String resultType) {
        _resultType = resultType;
    }

    public String getInfo() {
        return _info;
    }

    public void setInfo(String info) {
        _info = info;
    }

    public Map<String, Song> getSongs() {
        return _songs;
    }

    public void setSongs(Map<String, Song> songs) {
        _songs = songs;
    }
}
