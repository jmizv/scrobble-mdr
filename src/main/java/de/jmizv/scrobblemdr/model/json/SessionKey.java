package de.jmizv.scrobblemdr.model.json;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SessionKey implements Serializable {

    @SerializedName("session.name") // TODO nested: get the values from level lower.
    private String _name;
    @SerializedName("session.key")
    private String _key;
    @SerializedName("session.subscriber")
    private int _subscriber;

    public SessionKey() {
    }

    public String getName() {
        return _name;
    }

    public void setName(String name) {
        _name = name;
    }

    public String getKey() {
        return _key;
    }

    public void setKey(String key) {
        _key = key;
    }

    public int getSubscriber() {
        return _subscriber;
    }

    public void setSubscriber(int subscriber) {
        _subscriber = subscriber;
    }
}
