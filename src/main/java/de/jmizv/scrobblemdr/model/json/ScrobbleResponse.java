package de.jmizv.scrobblemdr.model.json;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class ScrobbleResponse implements Serializable {

    @SerializedName("scrobbles")
    private ScrobbleWrap _scrobbles;

    public ScrobbleWrap getScrobbles() {
        return _scrobbles;
    }

    public void setScrobbles(ScrobbleWrap scrobbles) {
        _scrobbles = scrobbles;
    }

    public int ignored() {
        return _scrobbles.getAttr().get("ignored");
    }

    public int accepted() {
        return _scrobbles.getAttr().get("accepted");
    }

    public List<Scrobble> ignoredScrobbles() {
        return _scrobbles.getScrobbles().stream()
                .filter(ScrobbleResponse::ignoredMessagePresent)
                .toList();
    }

    private static boolean ignoredMessagePresent(Scrobble scrobble) {
        return scrobble.getIgnoredMessage().getText() != null && !scrobble.getIgnoredMessage().getText().isEmpty();
    }

    public void listCorrections() {
        StringBuilder sb = new StringBuilder();
        _scrobbles.getScrobbles().stream()
                .flatMap(p -> Stream.of(p.getArtist(), p.getAlbum(), p.getTrack(), p.getAlbumArtist()))
                .filter(p -> !p.getCorrected().equals("0"))
                .forEach(obj -> sb.append(obj).append("\n"));
        if (!sb.isEmpty()) {
            System.err.println(sb);
        }
    }

    @Override
    public String toString() {
        return "ScrobbleResponse{" +
               "_scrobbles=" + _scrobbles +
               '}';
    }

    public static class ScrobbleWrap implements Serializable {

        @SerializedName("scrobble")
        private List<Scrobble> _scrobbles;

        @SerializedName("@attr")
        private Map<String, Integer> _attr;

        public List<Scrobble> getScrobbles() {
            return _scrobbles;
        }

        public void setScrobbles(List<Scrobble> scrobbles) {
            _scrobbles = scrobbles;
        }

        public Map<String, Integer> getAttr() {
            return _attr;
        }

        public void setAttr(Map<String, Integer> attr) {
            _attr = attr;
        }

        @Override
        public String toString() {
            return "ScrobbleWrap{" +
                   "_scrobbles=" + _scrobbles +
                   ", _attr=" + _attr +
                   '}';
        }
    }

    public static class Scrobble implements Serializable {
        @SerializedName("artist")
        private FieldItem _artist;
        @SerializedName("album")
        private FieldItem _album;
        @SerializedName("track")
        private FieldItem _track;
        @SerializedName("ignoredMessage")
        private IgnoredMessage _ignoredMessage;
        @SerializedName("albumArtist")
        private FieldItem _albumArtist;
        @SerializedName("timestamp")
        private String _timestamp;

        public FieldItem getArtist() {
            return _artist;
        }

        public void setArtist(FieldItem artist) {
            _artist = artist;
        }

        public FieldItem getAlbum() {
            return _album;
        }

        public void setAlbum(FieldItem album) {
            _album = album;
        }

        public FieldItem getTrack() {
            return _track;
        }

        public void setTrack(FieldItem track) {
            _track = track;
        }

        public IgnoredMessage getIgnoredMessage() {
            return _ignoredMessage;
        }

        public void setIgnoredMessage(IgnoredMessage ignoredMessage) {
            _ignoredMessage = ignoredMessage;
        }

        public FieldItem getAlbumArtist() {
            return _albumArtist;
        }

        public void setAlbumArtist(FieldItem albumArtist) {
            _albumArtist = albumArtist;
        }

        public String getTimestamp() {
            return _timestamp;
        }

        public void setTimestamp(String timestamp) {
            _timestamp = timestamp;
        }

        @Override
        public String toString() {
            return "Scrobble{" +
                   "_ignoredMessage=" + _ignoredMessage +
                   ", _artist=" + _artist +
                   ", _track=" + _track +
                   ", _timestamp='" + _timestamp + '\'' +
                   '}';
        }
    }

    public static class IgnoredMessage implements Serializable {
        @SerializedName("code")
        private String _code;
        @SerializedName("@text")
        private String _text;

        public String getCode() {
            return _code;
        }

        public void setCode(String code) {
            _code = code;
        }

        public String getText() {
            return _text;
        }

        public void setText(String text) {
            _text = text;
        }

        @Override
        public String toString() {
            return "IgnoredMessage{" +
                   "_code='" + _code + '\'' +
                   ", _text='" + _text + '\'' +
                   '}';
        }
    }

    public static class FieldItem implements Serializable {
        @SerializedName("corrected")
        private String _corrected;
        @SerializedName("@text")
        private String _text;

        public String getCorrected() {
            return _corrected;
        }

        public void setCorrected(String corrected) {
            _corrected = corrected;
        }

        public String getText() {
            return _text;
        }

        public void setText(String text) {
            _text = text;
        }

        @Override
        public String toString() {
            return "FieldItem{" +
                   "_corrected='" + _corrected + '\'' +
                   ", _text='" + _text + '\'' +
                   '}';
        }
    }
}
