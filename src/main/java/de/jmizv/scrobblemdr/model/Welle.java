package de.jmizv.scrobblemdr.model;

public enum Welle {

    MDR_SACHSEN(4),
    MDR_SACHSEN_ANHALT(5),
    MDR_THUERINGEN(6);

    private final int _id;

    Welle(int id) {
        _id = id;
    }

    public static Welle ofId(int id) {
        for (Welle welle : values()) {
            if (welle.getId() == id) {
                return welle;
            }
        }
        throw new IllegalArgumentException("Welle with id " + id + " does not exist.");
    }

    public static Welle ofName(String name) {
        name = normalize(name);
        for (Welle welle : values()) {
            if (normalize(welle.name()).equals(name)) {
                return welle;
            }
        }
        throw new IllegalArgumentException("Welle with name \"" + name + "\" could not be found.");
    }

    private static String normalize(String input) {
        return input.replace(" ", "")
                .replace("-", "")
                .replace("_", "")
                .toLowerCase();
    }

    public int getId() {
        return _id;
    }
}
