package de.jmizv.scrobblemdr.model;

import de.jmizv.scrobblemdr.entity.SongEntity;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ScrobbleToSend {

    /**
     * For some reason, the API adds double instead of single quotation marks. This is a list of known
     * words that should be replaced if the default logic does not work.
     */
    private static final Map<String, String> REPLACEMENTS = mapOf(
            "et\"s", "et's",
            "othing\"s", "othing's",
            "verything\"s", "verything's",
            "omething\"s", "omething's",
            "veryone\"s", "veryone's",
            "1\"s", "1's",
            "on\"t", "on't",
            "an\"t", "an't",
            "ouldn\"t", "ouldn't",
            "I\"ll", "I'll",
            "I\"m", "I'm",
            "I\"d", "I'd",
            "ou\"re", "ou're",
            "ou\"d", "ou'd",
            "he\"s", "he's",
            "He\"s", "He's",
            "It\"s", "It's",
            "it\"s", "it's",
            "O\"clock", "O'clock",
            "o\"clock", "o'clock",
            "ivin\"", "ivin'",
            "ovin\"", "ovin'",
            "ookin\"", "ookin'",
            "uttin\"", "uttin'",
            "in\"t", "in't",
            "L\"Italiano", "L'Italiano",
            "d\"Arban", "d'Arban",
            "reamin\"", "reamin'",
            "N\"ou", "N'ou",
            "\"69", "'69",
            "Rock\"n\"Roll", "Rock'n'Roll",
            "Rock \"n\" Roll", "Rock 'n' Roll",
            "haven\"t", "haven't",
            "nothin\"", "nothin'",
            "at\"s", "at's",
            "ove\"s", "ove's",
            "arlin\"", "arlin'",
            "alkin\"", "alkin'",
            "ittin\"", "ittin'",
            "d\"amour", "d'amour",
            "mo\"s", "mo's",
            "iro\"s", "iro's",
            "lin\"s", "lin's",
            "yin\"", "yin'",
            "oein\"", "oein'",
            "avy\"s", "avy's",
            "\"74 - \"75", "'74 - '75",
            "onight\"s", "onight's",
            "man\"s", "man's",
            "fallin\"", "fallin'",
            "ora\"s", "ora's",
            "m\"app","m'app",
            "Rap\" t", "Rap' t"

            );

    private String _artist;
    private String _track;
    private Instant _timestamp;
    private String _album;
    private String _albumArtist;

    private long _duration;

    public ScrobbleToSend() {
    }

    public String getArtist() {
        return _artist;
    }

    public ScrobbleToSend setArtist(String artist) {
        _artist = artist;
        return this;
    }

    public String getTrack() {
        return _track;
    }

    public ScrobbleToSend setTrack(String track) {
        _track = track;
        return this;
    }

    public Instant getTimestamp() {
        return _timestamp;
    }

    public ScrobbleToSend setTimestamp(Instant timestamp) {
        _timestamp = timestamp;
        return this;
    }

    public String getAlbum() {
        return _album;
    }

    public ScrobbleToSend setAlbum(String album) {
        _album = album;
        return this;
    }

    public long getDuration() {
        return _duration;
    }

    public ScrobbleToSend setDuration(long duration) {
        _duration = duration;
        return this;
    }

    public String getAlbumArtist() {
        return _albumArtist;
    }

    public ScrobbleToSend setAlbumArtist(String albumArtist) {
        _albumArtist = albumArtist;
        return this;
    }

    public static ScrobbleToSend of(SongEntity entity) {
        ScrobbleToSend scrobble = new ScrobbleToSend();
        scrobble._album = fixSpelling(entity.getTontraeger());
        scrobble._artist = fixSpelling(entity.getInterpret());
        scrobble._track = fixSpelling(entity.getTitle());
        scrobble._timestamp = entity.getStarttime();
        if (entity.getDuration() != null) {
            scrobble._duration = durationAsLong(entity.getDuration());
        }
        return scrobble;
    }

    static long durationAsLong(String input) {
        Objects.requireNonNull(input, "Input should not be null");
        if (input.isEmpty()) {
            throw new IllegalArgumentException("Input is empty");
        }
        var splitted = input.split(":");
        long result = Integer.parseInt(splitted[2]);
        result += 60L * Integer.parseInt(splitted[1]);
        result += 60L * 60L * Integer.parseInt(splitted[0]);
        return result;
    }

    static Map<String, String> mapOf(String... values) {
        if (values.length % 2 != 0) {
            throw new IllegalArgumentException("Only even amount of items is supported for creating a map from it.");
        }
        Map<String, String> result = new HashMap<>();
        for (int i = 0; i < values.length; i += 2) {
            if (result.containsKey(values[i])) {
                throw new IllegalArgumentException("Key " + values[i] + " already exists.");
            }
            result.put(values[i], values[i + 1]);
        }
        return result;
    }

    static String fixSpelling(String input) {
        if (input == null) {
            return null;
        }
        var indexOfQuotation = input.indexOf("\"");
        if (indexOfQuotation == input.lastIndexOf("\"") && indexOfQuotation >= 0) {
            input = input.replace("\"", "'");
        }

        for (Map.Entry<String, String> replacements : REPLACEMENTS.entrySet()) {
            input = input.replace(replacements.getKey(), replacements.getValue());
        }
        return input;
    }
}
