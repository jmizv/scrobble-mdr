package de.jmizv.scrobblemdr.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Song implements Serializable {

    @SerializedName("status")
    private String _status;
    @SerializedName("id_titel")
    private String _idTitel;
    @SerializedName("title")
    private String _title;
    @SerializedName("subtitle")
    private String _subtitle;
    @SerializedName("starttime")
    private String _starttime;
    @SerializedName("author")
    private String _author;
    @SerializedName("av_next_id")
    private String _avNextId;
    @SerializedName("duration")
    private String _duration;
    @SerializedName("interpret")
    private String _interpret;
    @SerializedName("kurzinfo")
    private String _kurzinfo;
    @SerializedName("metadatentext")
    private String _metadatentext;
    @SerializedName("interpret_url")
    private String _interpretUrl;
    @SerializedName("artist_image_id")
    private Object _artistImageId;
    @SerializedName("transmissiontype")
    private String _transmissiontype;
    @SerializedName("audioasset")
    private String _audioasset;
    @SerializedName("komponist")
    private String _komponist;
    @SerializedName("label")
    private String _label;
    @SerializedName("tontraeger")
    private String _tontraeger;

    public String getStatus() {
        return _status;
    }

    public void setStatus(String status) {
        _status = status;
    }

    public String getIdTitel() {
        return _idTitel;
    }

    public void setIdTitel(String idTitel) {
        _idTitel = idTitel;
    }

    public String getTitle() {
        return _title;
    }

    public void setTitle(String title) {
        _title = title;
    }

    public String getSubtitle() {
        return _subtitle;
    }

    public void setSubtitle(String subtitle) {
        _subtitle = subtitle;
    }

    public String getStarttime() {
        return _starttime;
    }

    public void setStarttime(String starttime) {
        _starttime = starttime;
    }

    public String getAuthor() {
        return _author;
    }

    public void setAuthor(String author) {
        _author = author;
    }

    public String getAvNextId() {
        return _avNextId;
    }

    public void setAvNextId(String avNextId) {
        _avNextId = avNextId;
    }

    public String getDuration() {
        return _duration;
    }

    public void setDuration(String duration) {
        _duration = duration;
    }

    public String getInterpret() {
        return _interpret;
    }

    public void setInterpret(String interpret) {
        _interpret = interpret;
    }

    public String getKurzinfo() {
        return _kurzinfo;
    }

    public void setKurzinfo(String kurzinfo) {
        _kurzinfo = kurzinfo;
    }

    public String getMetadatentext() {
        return _metadatentext;
    }

    public void setMetadatentext(String metadatentext) {
        _metadatentext = metadatentext;
    }

    public String getInterpretUrl() {
        return _interpretUrl;
    }

    public void setInterpretUrl(String interpretUrl) {
        _interpretUrl = interpretUrl;
    }

    public Object getArtistImageId() {
        return _artistImageId;
    }

    public void setArtistImageId(Object artistImageId) {
        _artistImageId = artistImageId;
    }

    public String getTransmissiontype() {
        return _transmissiontype;
    }

    public void setTransmissiontype(String transmissiontype) {
        _transmissiontype = transmissiontype;
    }

    public String getAudioasset() {
        return _audioasset;
    }

    public void setAudioasset(String audioasset) {
        _audioasset = audioasset;
    }

    public String getKomponist() {
        return _komponist;
    }

    public void setKomponist(String komponist) {
        _komponist = komponist;
    }

    public String getLabel() {
        return _label;
    }

    public void setLabel(String label) {
        _label = label;
    }

    public String getTontraeger() {
        return _tontraeger;
    }

    public void setTontraeger(String tontraeger) {
        _tontraeger = tontraeger;
    }

    @Override
    public String toString() {
        return "Song{" +
               "idTitel='" + _idTitel + '\'' +
               ", interpret='" + _interpret + '\'' +
               ", title='" + _title + '\'' +
               ", duration='" + _duration + '\'' +
               '}';
    }
}
