package de.jmizv.scrobblemdr.service;

import com.google.gson.Gson;
import de.jmizv.scrobblemdr.entity.SongEntity;
import de.jmizv.scrobblemdr.model.Song;
import de.jmizv.scrobblemdr.model.SongList;
import de.jmizv.scrobblemdr.model.Welle;
import de.jmizv.scrobblemdr.repository.SongRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

@Component
public class TitleService {

    private final SongRepository _songRepository;

    public TitleService(@Autowired SongRepository songRepository) {
        _songRepository = songRepository;
    }

    public List<Song> songs(Welle radioStation, int amount, LocalDate start) throws IOException, InterruptedException {
        String uri = "https://www.mdr.de/scripts4/titellisten/xmlresp-index.do?output=json&idwelle=%welle%&amount=%amount%&startdate=%start%"
                .replace("%welle%", String.valueOf(radioStation.getId()))
                .replace("%amount%", String.valueOf(amount))
                .replace("%start%", start.toString().replace("-", ""));
        HttpClient client = HttpClient.newHttpClient();

        HttpRequest httpRequest = HttpRequest.newBuilder()
                .uri(URI.create(uri))
                .header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/114.0")
                .header("Origin", "https://www.mdr.de")
                .header("Accept", "application/json, text/javascript, */*; q=0.01")
                .header("Accept-Language", "de,en-US;q=0.7,en;q=0.3")
                .header("Accept-Encoding", "gzip, deflate, br")
                .header("X-Requested-With", "XMLHttpRequest")
                .header("DNT", "1")
                .header("Referer", "https://www.mdr.de/mdr-sachsen-anhalt/titelliste-mdr-sachsen-anhalt--102.html")
                .header("Sec-Fetch-Dest", "empty")
                .header("Sec-Fetch-Mode", "cors")
                .header("Sec-Fetch-Site", "same-origin")
                .header("Sec-GPC", "1")
                .header("TE", "trailers")
                .GET()
                .build();
        HttpResponse<InputStream> send = client.send(httpRequest, HttpResponse.BodyHandlers.ofInputStream());
        try (InputStream body = send.body()) {
            long startOfDownload = System.currentTimeMillis();
            byte[] contentAsBytes = body.readAllBytes();
            System.out.println("Downloaded " + contentAsBytes.length + " bytes in " + (System.currentTimeMillis() - startOfDownload) + "ms.");
            return List.copyOf(new Gson().fromJson(new String(contentAsBytes), SongList.class).getSongs().values());
        }
    }

    public SongEntity create(SongEntity entity) {
        return _songRepository.save(entity);
    }

    public Iterable<SongEntity> getNotScrobbledTracks(Welle welle) {
        return _songRepository.findByScrobbledAndWelleFkId(false, welle.getId(), Sort.by("starttime"));
    }

    public LocalDate getMaxStarttimeOfTitle(Welle welle) {
        var max = _songRepository.findMaxStarttime(welle.getId());
        if (max == null) {
            return null;
        }
        return LocalDate.ofInstant(max, ZoneId.of("Europe/Berlin"));
    }

    public void setScrobbled(List<Long> idsThatHasBeenScrobbled) {
        Iterable<SongEntity> allById = _songRepository.findAllById(idsThatHasBeenScrobbled);
        var list = new ArrayList<SongEntity>();
        for (SongEntity entity : allById) {
            entity.setScrobbled(true);
            list.add(entity);
        }
        _songRepository.saveAll(list);
    }

    public void setIgnored(List<Long> ignoredScrobbles) {
        Iterable<SongEntity> allById = _songRepository.findAllById(ignoredScrobbles);
        var list = new ArrayList<SongEntity>();
        for (SongEntity entity : allById) {
            entity.setIgnored(true);
            list.add(entity);
        }
        _songRepository.saveAll(list);
    }
}
