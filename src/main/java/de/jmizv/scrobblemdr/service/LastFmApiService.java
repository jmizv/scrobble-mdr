package de.jmizv.scrobblemdr.service;

import com.google.gson.Gson;
import de.jmizv.scrobblemdr.model.ScrobbleToSend;
import de.jmizv.scrobblemdr.model.Welle;
import de.jmizv.scrobblemdr.model.json.ScrobbleResponse;
import de.jmizv.scrobblemdr.model.json.SessionKey;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpHeaders;
import java.net.http.HttpRequest;

import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

@Component
public class LastFmApiService {

    @Value("${lastfm.api.key}")
    private String _apiKey;
    @Value("${lastfm.api.secret}")
    private String _apiSecret;
    @Value("#{${lastfm.api.token}}")
    private Map<Integer, String> _tokenMap;
    @Value("#{${lastfm.api.session-key}}")
    private Map<Integer, String> _sessionKeyMap;

    public String apiAuth() {
        return "https://www.last.fm/api/auth/?api_key=" + _apiKey;
    }

    public SessionKey authGetSession(Welle welle) throws IOException, InterruptedException {
        String sign;
        var token = _tokenMap.get(welle.getId());
        try {
            String payload = apiKeyForSignature() + "methodauth.getSession" + "token" + token;
            sign = sign(payload);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Could not sign api call.", e);
        }

        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://ws.audioscrobbler.com/2.0/?format=json&method=auth.getSession&token=" + token + "&api_key=" + _apiKey + "&api_sig=" + sign))
                .GET().build();

        HttpResponse<InputStream> send = client.send(request, HttpResponse.BodyHandlers.ofInputStream());
        try (var responseInputStream = send.body()) {
            if (send.statusCode() / 100 > 2) {
                throw new RuntimeException("HTTP " + send.statusCode() + "\n" + new String(responseInputStream.readAllBytes()));
            }
            var json = new String(responseInputStream.readAllBytes());
            return new Gson().fromJson(json, SessionKey.class);
        }
    }

    private String emptyStringIfNull(String input) {
        return input == null ? "" : input;
    }

    private static String encode(String in) {
        return URLEncoder.encode(in, StandardCharsets.UTF_8);
    }

    public ScrobbleResponse scrobble(Welle welle, List<ScrobbleToSend> scrobbles) throws IOException, InterruptedException {
        if (scrobbles.size() > 50) {
            throw new IllegalArgumentException("Cannot scrobble more than 50 tracks at once.");
        }
        if (scrobbles.isEmpty()) {
            // nothing to do here.
            return new ScrobbleResponse();
        }
        Map<String, String> params = new TreeMap<>();
        params.put("api_key", _apiKey);
        params.put("method", "track.scrobble");
        params.put("sk", _sessionKeyMap.get(welle.getId()));

        for (int i = 0; i < scrobbles.size(); ++i) {
            ScrobbleToSend scrobbleToSend = scrobbles.get(i);
            params.put("artist[" + i + "]", scrobbleToSend.getArtist());
            params.put("track[" + i + "]", scrobbleToSend.getTrack());
            params.put("album[" + i + "]", emptyStringIfNull(scrobbleToSend.getAlbum()));
            params.put("albumArtist[" + i + "]", emptyStringIfNull(scrobbleToSend.getAlbumArtist()));
            params.put("timestamp[" + i + "]", String.valueOf(scrobbleToSend.getTimestamp().toEpochMilli() / 1000));
        }

        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request;
        try {
            String payload = toScrobblePayload(params, sign(params));
            request = HttpRequest.newBuilder()
                    .uri(URI.create("http://ws.audioscrobbler.com/2.0/?format=json"))
                    .POST(HttpRequest.BodyPublishers.ofString(payload))
                    .build();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Could not calculate signature.", e);
        }
        HttpResponse<InputStream> send = client.send(request, HttpResponse.BodyHandlers.ofInputStream());
        try (var responseInputStream = send.body()) {
            String response = new String(responseInputStream.readAllBytes());
            if (send.statusCode() / 100 > 2) {
                HttpHeaders headers = send.headers();
                String headersAsString = headers.map().entrySet().stream().map(k -> k.getKey() + ": " + k.getValue()).collect(Collectors.joining("; "));
                throw new RuntimeException("HTTP " + send.statusCode() + ": Could not successfully send scrobble request: " + response + "\n" + headersAsString);
            }
            return new Gson().fromJson(response, ScrobbleResponse.class);
        }
    }

    private String toScrobblePayload(Map<String, String> params, String sign) {
        params.put("api_sig", sign);
        return params.entrySet().stream().map(k -> k.getKey() + "=" + encode(k.getValue())).collect(Collectors.joining("&"));
    }

    private String apiKeyForSignature() {
        return "api_key" + _apiKey;
    }

    /**
     * Appends the api secret to the payload, generates MD5 hash of that string and converts the result to a Hex string which is returned.
     *
     * @return the signature that should be used in Last.FM api calls.
     */
    private String sign(String payload) throws NoSuchAlgorithmException {
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        byte[] digest = md5.digest((payload + _apiSecret).getBytes(StandardCharsets.UTF_8));

        StringBuilder sb = new StringBuilder();
        for (byte b : digest) {
            sb.append(String.format("%02X", b));
        }

        return sb.toString();
    }

    private String sign(Map<String, String> params) throws NoSuchAlgorithmException {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String> entry : params.entrySet()) {
            sb.append(entry.getKey()).append(entry.getValue());
        }
        return sign(sb.toString());
    }
}
