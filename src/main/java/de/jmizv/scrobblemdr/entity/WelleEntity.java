package de.jmizv.scrobblemdr.entity;

import com.google.gson.annotations.SerializedName;
import jakarta.persistence.*;

import java.io.Serializable;

@Entity
@Table(name = "welle")
public class WelleEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long _id;
    @Column(name = "name")
    private String _name;
    @Column(name = "lastfmusername")
    private String _lastfmusername;

    public WelleEntity() {
    }

    public Long getId() {
        return _id;
    }

    public void setId(Long id) {
        _id = id;
    }

    public String getName() {
        return _name;
    }

    public void setName(String name) {
        _name = name;
    }

    public String getLastfmusername() {
        return _lastfmusername;
    }

    public void setLastfmusername(String lastfmusername) {
        _lastfmusername = lastfmusername;
    }
}
