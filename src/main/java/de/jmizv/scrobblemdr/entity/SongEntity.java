package de.jmizv.scrobblemdr.entity;

import de.jmizv.scrobblemdr.model.Song;
import jakarta.persistence.*;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

@Entity
@Table(name = "song")
public class SongEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long _id;
    @Column(name = "welle_fk_id")
    private Long welleFkId;
    private transient WelleEntity _welle;
    @Column(name = "id_titel")
    private String _idTitel;
    @Column(name = "title")
    private String _title;
    @Column(name = "subtitle")
    private String _subtitle;
    @Column(name = "starttime")
    private Instant starttime;
    @Column(name = "author")
    private String _author;
    @Column(name = "av_next_id")
    private String _avNextId;
    @Column(name = "duration")
    private String _duration;
    @Column(name = "interpret")
    private String _interpret;
    @Column(name = "kurzinfo")
    private String _kurzinfo;
    @Column(name = "metadatentext")
    private String _metadatentext;
    @Column(name = "interpret_url")
    private String _interpretUrl;
    /*@Column(name = "")
    private Object _artistImageId;*/
    @Column(name = "transmissiontype")
    private String _transmissiontype;
    @Column(name = "audioasset")
    private String _audioasset;
    @Column(name = "komponist")
    private String _komponist;
    @Column(name = "label")
    private String _label;
    @Column(name = "tontraeger")
    private String _tontraeger;
    @Column(name = "scrobbled")
    private boolean scrobbled;
    @Column(name = "ignored")
    private boolean ignored;

    private static final DateTimeFormatter PARSER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss", Locale.GERMAN);

    public SongEntity() {
    }

    public Long getId() {
        return _id;
    }

    public void setId(Long id) {
        _id = id;
    }

    public Long getWelleFkId() {
        return welleFkId;
    }

    public void setWelleFkId(Long welleFkId) {
        this.welleFkId = welleFkId;
    }

    public WelleEntity getWelle() {
        return _welle;
    }

    public void setWelle(WelleEntity welle) {
        _welle = welle;
    }

    public String getIdTitel() {
        return _idTitel;
    }

    public void setIdTitel(String idTitel) {
        _idTitel = idTitel;
    }

    public String getTitle() {
        return _title;
    }

    public void setTitle(String title) {
        _title = title;
    }

    public String getSubtitle() {
        return _subtitle;
    }

    public void setSubtitle(String subtitle) {
        _subtitle = subtitle;
    }

    public Instant getStarttime() {
        return starttime;
    }

    public void setStarttime(Instant starttime) {
        this.starttime = starttime;
    }

    public String getAuthor() {
        return _author;
    }

    public void setAuthor(String author) {
        _author = author;
    }

    public String getAvNextId() {
        return _avNextId;
    }

    public void setAvNextId(String avNextId) {
        _avNextId = avNextId;
    }

    public String getDuration() {
        return _duration;
    }

    public void setDuration(String duration) {
        _duration = duration;
    }

    public String getInterpret() {
        return _interpret;
    }

    public void setInterpret(String interpret) {
        _interpret = interpret;
    }

    public String getKurzinfo() {
        return _kurzinfo;
    }

    public void setKurzinfo(String kurzinfo) {
        _kurzinfo = kurzinfo;
    }

    public String getMetadatentext() {
        return _metadatentext;
    }

    public void setMetadatentext(String metadatentext) {
        _metadatentext = metadatentext;
    }

    public String getInterpretUrl() {
        return _interpretUrl;
    }

    public void setInterpretUrl(String interpretUrl) {
        _interpretUrl = interpretUrl;
    }

    public String getTransmissiontype() {
        return _transmissiontype;
    }

    public void setTransmissiontype(String transmissiontype) {
        _transmissiontype = transmissiontype;
    }

    public String getAudioasset() {
        return _audioasset;
    }

    public void setAudioasset(String audioasset) {
        _audioasset = audioasset;
    }

    public String getKomponist() {
        return _komponist;
    }

    public void setKomponist(String komponist) {
        _komponist = komponist;
    }

    public String getLabel() {
        return _label;
    }

    public void setLabel(String label) {
        _label = label;
    }

    public String getTontraeger() {
        return _tontraeger;
    }

    public void setTontraeger(String tontraeger) {
        _tontraeger = tontraeger;
    }

    public boolean isScrobbled() {
        return scrobbled;
    }

    public void setScrobbled(boolean scrobbled) {
        this.scrobbled = scrobbled;
    }

    public boolean isIgnored() {
        return ignored;
    }

    public void setIgnored(boolean ignored) {
        this.ignored = ignored;
    }

    public static SongEntity of(Song song, long welleId) {
        var songEntity = new SongEntity();
        songEntity.setAudioasset(song.getAudioasset());
        songEntity.setAuthor(song.getAuthor());
        songEntity.setAvNextId(song.getAvNextId());
        songEntity.setInterpret(song.getInterpret());
        songEntity.setDuration(song.getDuration());
        songEntity.setKurzinfo(song.getKurzinfo());
        songEntity.setKomponist(song.getKomponist());
        songEntity.setIdTitel(song.getIdTitel());
        songEntity.setInterpretUrl(song.getInterpretUrl());
        songEntity.setLabel(song.getLabel());
        songEntity.setMetadatentext(song.getMetadatentext());
        songEntity.setTransmissiontype(song.getTransmissiontype());
        songEntity.setWelleFkId(welleId);
        songEntity.setTontraeger(song.getTontraeger());
        songEntity.setTitle(song.getTitle());
        songEntity.setSubtitle(song.getSubtitle());
        LocalDateTime localDateTime = LocalDateTime.parse(song.getStarttime(), PARSER);
        songEntity.setStarttime(localDateTime.atZone(ZoneId.of("Europe/Berlin")).toInstant());
        return songEntity;
    }
}
