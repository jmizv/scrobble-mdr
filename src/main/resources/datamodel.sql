CREATE TABLE welle(
  id serial PRIMARY KEY,
  -- Name of the radio station
  name varchar(255) not null,
  -- The last.fm user name to which the songs should be scrobbled
  lastfmusername varchar(255)
);

-- For now only one station.
INSERT INTO welle (id, name, lastfmusername) VALUES
(4,'MDR Sachsen','mdrsachsen'),
(5,'MDR Sachsen-Anhalt','mdrsaxenanhalt'),
(6,'MDR Thüringen','mdrthueringen');

-- TODO needs to be normalized ...
CREATE TABLE song(
  id serial PRIMARY KEY,
  welle_fk_id int not null,
  id_titel varchar(32) not null,
  title varchar(255) not null,
  subtitle varchar(255),
  starttime timestamp not null,
  author varchar(255),
  av_next_id varchar(255),
  duration varchar(32) not null,
  interpret varchar(255) not null,
  kurzinfo varchar(255),
  metadatentext varchar(255),
  interpret_url varchar(255),
  transmissiontype varchar(255),
  audioasset varchar(255),
  komponist varchar(255),
  label varchar(255),
  tontraeger varchar(255),
  scrobbled boolean not null default false,
  ignored boolean not null default false,
  CONSTRAINT welle_fk FOREIGN KEY (welle_fk_id) REFERENCES welle(id)
);